const express = require('express');

// Require logger.js
const logger = require('./looger');
var bodyParser = require('body-parser');
const fs = require('fs');
const app = express();

const port = 3000;
const host = "localhost";

let bodyParserS = {
    urlencoded: { limit: '50mb', extended: true, parameterLimit: 50000 },
    json: { limit: '50mb' }
}

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json(bodyParserS.json));
// Dummy Express GET call
app.get('/',(req,res) => {
    res.send("Hello World!");
    logger.info("Server Sent A Hello World!");
})

// Introduce error by using undefined variable 'y'
app.get('/calc',(req,res) => {
    const x = y + 10;
    res.send(x.toString());
})

app.get('/log/:date/:type',(req,res) => {
    let date = req.params.date;
    let type = req.params.type;
    res.sendFile(__dirname + '/logs/' + date + '-logs/server-' + type + '.log');
})

// Capture 500 errors
app.use((err,req,res,next) => {
res.status(500).send({"status":false,"message":"Something went wrong","error" : err.message});
    let errorObj = {
        "error_status" : 500,
        "statusMessage" : res.statusMessage,
        "message" :  err.message,
        "originalUrl" : req.originalUrl,
        "method" : req.method,
        "req.ip" : req.ip
    }
   logger.error("Could not perform the calculation!",errorObj);
})

// Capture 404 erors
app.use((req,res,next) => {
    res.status(404).send("PAGE NOT FOUND");
    let infoObj = {
        "error_status" : 404,
        "statusMessage" : res.statusMessage,
        "originalUrl" : req.originalUrl,
        "method" : req.method,
        "req.ip" : req.ip
    }
   logger.error("PAGE NOT FOUND:",infoObj);
})


// Run the server
app.listen(port, () => {
    console.log("Server started...");
})
